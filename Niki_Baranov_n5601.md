# Nikita (Niki) Baranov

![Niki Baranov](src/niki_2018_250x333.jpg "Niki Baranov")

## Contact info
	Haperontie 3 B 38, 40640 Jyväskylä, Finland 
	☎ +358 44 960 8041
	E-mail: niki.baranov@gmail.com
	Skype: niki.baranov
	Age: 36
	Full driver's license (EU-FIN) ABC
    https://www.linkedin.com/in/nikibaranov


# Work Experience
	Finnish Defense Forces, UNIFIL (Lebanon)
	11.2018 – 03.2019 (4m 22d)
- 	CIS (Communication & Information System) NCO, Sergeant
---
	LMA Gaming Ltd. (Cyprus) Technical Development Manager / Website administrator
	2014 – 2016
- My position would rather resemble that of a CTO as all technical and otherwise IT related issues and decisions went through my desk first. Development and other productivity doubled due to streamlined, optimized and better structured workflow.
- Budgeting, managing and setting up IT and infrastructural systems to ensure cost effective and reliable operations of the company.
- Ensuring that IT and website development projects are managed effectively and met within agreed timescales.
- Finding and utilizing new solutions for email marketing.
- Managing and leading the IT team (3rd party company) to support service solutions/delivery and the growth of the business.
- Restructuring development and communications processes between departments.
---

	LMA Gaming Ltd. (Cyprus) Head Developer / IT / Website administrator
	2010 – 04.2013	
- Developing and maintaining over 40 online gaming (and/or related) websites (PHP, HTML, SQL, Java)
- Integrating with systems provided by partnering iGaming operators (NetEnt, Aspire Global, DragonFish, Functional Games).
- Finding and applying systems to maximize speed and performance of websites.
- Setting up procedures and tools to maximize development output.
---

	LMA Gaming Ltd. (Cyprus) Developer / IT / Website administrator
	2009 – 2010	
- Integrating with systems provided by partnering iGaming operators (Aspire Global).
- Developing and maintaining 30 online gaming (and/or related) websites (PHP, HTML, SQL, Java)
-nWriting / creating content for the websites as well as execution and planning of SEO activities. 
- Development of new websites to support overall business growth.
---

	JMR Marketing Ltd. (Cyprus) Website administrator / IT
	2008 – 2009	
- Developing and maintaining 20 online gaming (and/or related) websites using HTML as well as WordPress and Joomla platforms.
-vWriting / creating content for the websites as well as execution and planning of SEO activities. 
- Development of new websites to support overall business growth.
---

	Itella Oyj (Finland)
	2006 – 2008	
- Newspaper delivery.
---

	ATK Varikko Ky (Finland) IT support internship
	2004 – 2005	
- PC repair & maintenance & sales. LAN setup, server setup and configuration. House calls
---

	Pohjoisen Keski-Suomen Oppimiskeskus (POKSAT) (Finland) IT Support
	2001 – 2001	
- PC repair & maintenance, installation, optimization. LAN setup, server setup and
configuration. Helping other staff members with various computer related issues.


# Education
	JAMK University of Applied Sciences (Finland)
	Bachelor of Engineering (IT, 3.17)
	2017 – 2018

- Completing missing credits (Cybersecurity, Physics, PHP, Swedish for working life) and thesis project (PHP, PostgreSQL and JavaScript based application).
---
	JAMK University of Applied Sciences (Finland)
	Bachelor of Engineering (IT, did not graduate)
	2005 – 2008
- Studied C++, C#, Java, JavaScript, SQL, HTML, a bit of flash programming, and 3d graphics design.
---
	Finnish Defense Forces Military Service
	Corporal, Finnish Rapid Deployment Forces (FRDF), Marksman
	2003 – 2004
- NATO exercise “Battle Griffin” in February 2005 (Norway).
---
	Äänekosken Ammattikoulu (Trade School) (Finland)
	IT / Electronics mechanic.
	1998 – 2001	
---
	1998 – 2001	
	Äänekosken lukio (Upper Secondary School) (Finland)
	Generic courses for trade school degree.
	


# Other Activities & Experience
	2019
	Cybersecurity courses via Udemy, DevOps courses and otherwise updating my developer skillset.

	2018
	Learning Unity and C# for a personal game development project.

	2017
	Writing/coding the thesis project for Bachelor of Engineering Degree (JAMK), Refreshing programming skillsTraveling, Canada 2 months

	2016 – 2017
	Traveling & hobbies, South Africa 3 months, Canada 2 months, United States 2 months.

	2015
	FOWD conference in San Francisco & 10-day road trip in western United States. |

	2013 – 2014
	Traveling & hobbies.

	2010
	Website developing / moderation Developing and moderation of websites for various businesses, mainly using WordPres
	http://dna-paintball.co.za
	http;//kreaturepaintball.co.za

## Language Skills
	Finnish native
	English native
	Russian spoken good / written weak

## Other Skills	
	Adobe Photoshop – print and web graphics creation & basic photo & graphics manipulation, Adobe Creative Cloud toolkit, Microsoft Office, PHP, SQL, HTML & HTML5, CSS, WordPress, SEO & SEM, Email & SMS marketing. Cybersecurity basics.

## Interests & Hobbies	
	Cybersecurity, mobile programming, Downhill mountain biking & skiing, traveling, hiking, reading, new technologies, flight simulators, Unity3D, React, Node.js, DevOps

# Weather on Saturday
Jyväskylä weather forecast Saturday 21.09.2019 at 15:52:

![Jyväskylä weather forecast](src/weather_forecast_sat_21_09_2019.png "Jyväskylä weather forecast")

# Courses

| **Course Code** | **Name** | **Credits** | **Group**	| **Semester** | 
|-|-|-|-|-|
| [TTZW0410](http://asio.jamk.fi/pls/asio/asio_ectskuv1.kurssin_ks?ktun=TTZW0410&knro=&noclose=%20&lan=e "Git -version control and Gitlab -project management environment") |_Git -version control and Gitlab -project management environment_ | 1 | TTK19S1DEV | Autumn 2019 |

# Favorite movie
>"Because I was inverted" | [Top Gun](https://www.imdb.com/title/tt0092099/)

>[![Top Gun](https://img.youtube.com/vi/xa_z57UatDY/0.jpg)](https://youtu.be/xa_z57UatDY)

# How was my day?
😴🛏️🚽🚿☕🍳🍞📺🤓😊👍🤓🎶🤓🤓🤓🍚🥩📺🤓🤓🎶🎶😎😊👍✔️💤

# Harjoitustyo5 Update
Checking out the branching feature of Git
Main commands to handle branches are:

>git branch

>git branch name_of_the_new_branch

>git checkout name_of_the_branch_to_use

>git push --set-upstream origin name_of_the_branch_to_upload

>git merge --no-ff name_of_the_branch_to_merge_to_currently_used_branch 
>>dont forget to change the branch to the one to be merged to first
# Feature 1 what is Git diff?
Its a tool that allows the user to compare the changes between two versions of one or multiple files, commits etc.

---

##  Learning to use git diff simple test (cached files check)
Running a simple test as instructed below should give you the very basic understanding of using ```git diff```
1. create a file
2. add some text to it and save
3. git add the file
4. reopen the file & change some text
5. run git diff

---

## How to use **git diff** ?
All options and other info regarding diff can be found at Git Diff page:
https://git-scm.com/docs/git-diff
There are quite a few options as well as multiple ways of defining what to compare and how to show the result or whether the system should react in some specific way if changes are found. I will only go through few basic ways of using ```git diff```.

---

### Check "cached" files against possible local non-commited changes
To check how the files that are already added for the next commit differ from the latest version you currently have, you can simply use this, it will show all changes in all files in the currently "cached" commit aka "added" files.

![Added files](feature1/git_diff_between_multiple_files2.png)

>```git diff```


Image below shows the difference between  versions of two files that were already added and a more recent version of the same  files.

![Multiple Files](feature1/git_diff_between_multiple_files.png)

The result is pretty easy to understand:
  
    The system marks a & b versions of changed files with - and +
    Then menntiones the location of the change (line)
    it also shows a bit of the previous line

---

### Check between 2 commits
You can also compare two already made and uploaded commits by using the first 7 characters of their hash. Again this will show all changes to all files done in the commit in question.
>```git diff commit-id1 commit-id2```

![Check between 2 commits](feature1/git_diff_difference_in_commits.png)

---
### Compare 2 files
This allows to check for differences between 2 specific files even before adding them to commit or uploading.
>```git diff --no-index filename1 filename2```

The ```--no-index``` part is the command that allows you to check for "non-working" files, for example if you need to check against a file from a folder that is outside the repository.

![Difference between files command](feature1/git_diff_difference_between_files.png)

![Difference between files detailed](feature1/git_diff_difference_between_files2.png)

---

In addition to commands above, you can also check the for differences between specific files in different branches, like this:

```git diff``` branch_name1 brannch_name2 -- filename

![Difference  between branches](feature1/git_diff_difference_between_branches.png)

![Difference  between branches detailed](feature1/git_diff_difference_between_branches2.png)

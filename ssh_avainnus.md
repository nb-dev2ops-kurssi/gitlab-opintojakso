# SSH avainnus
Gitlabin ohjeessa manittiin että on suositeltavaa käyttää ed25519 eikä rsa avaimia joten:

```ssh-keygen -t ed25519 -C "n5601@student.jamk.fi"```
>![Generating new ed25519 key](src/pics/harj3/create_ed25519_key.png "Generating new ed25519 key")

Sitten joko syötetään avaimen tiedoston nimi tai käytetään oletusta, jos käytetään oletusta niin avainpari tallentuu (tässä tapauksessa) MAC käyttäjän ssh kansioon, muussa tapauksessa tiedosto tellentuu sinne minne käyttäjä määrittelee (tässä tapauksessa git-opintojakso kansioon), joten siirrän sen sinne ssh kansioon minne se kuuluukin, avaimia onn tosin mahdollista käyttää muualtakin.

```pbcopy < jamk_id_ed25519.pub```
>_Lisää avaimen MAC käyttäjälle, huom. Ei tarvitse määrittää erikseen onko pub vai ei_

```ssh-add avaimen_polku_ja_nimi```
>_kopioi julkisen avaimen leikepöydälle_

>![Adding new key to user](src/pics/harj3/add_new_key.png "Adding new key to user")


Tämän jälkeen **User settings** >> **SSH Keys** ja lisää uusi (kyseiseltä sivulta voi tarkastaa kaikki lisätyt avaimet)
>![Adding new key to GitLab profile](src/pics/harj3/add_new_key_to_gitlab.png "Adding new key to GitLab profile")


```ssh -T git@gitlab.labranet.jamk.fi```
>_Tarkistan vielä että kaikki toimii_
> ![Testing that new key works](src/pics/harj3/test_new_key.png "Testing that new key works")



# HTTP --> SSH
Tässä vaiheessa lisään myös SSH urlin repon asetuksiin kuten harjoituksen matskussa oheistettiin (ei pakko mutta haluan kokeilla)

```git remote set-url origin git@gitlab.labranet.jamk.fi:N5601/gitlab-opintojakso.git```
> ![Changing connection to SSH](src/pics/harj3/set_connection_to_ssh.png "Changing connection to SSH")


Kokeillaan vielä tehdä jokin pieni muutos ja ladata tiedosto repoon
> ![Testing new connection](src/pics/harj3/test_new_connection.png "Testing new connection")

**Homma toimii!**